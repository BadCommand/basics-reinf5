package array;

public class SameNumbers {

	public static boolean sameNumbers(int[] values1, int[] values2) {
		/*
		 * TODO Implement a method sameNumbers(), which tests whether the same numbers
		 * occur in the two given int arrays. E.g.: {1, 2} , {1, 1, 2} -> true /*
		 */

		int n1 = values1.length;
		int n2 = values2.length;
		int j;
		int counter = 0;

		boolean isTwice = false;

		if (n1 != 0 && n2 != 0) {
			if (n1 == n2) {
				for (int i = 0; i < n1; i++) {
					j = 0;
					while (j < n2) {
						if (values1[i] == values2[j]) {
							counter++;
						}
						j++;
					}
				}
				if (counter >= n1) {
					isTwice = true;
				}
			} else if (n1 > n2) {
				for (int i = 0; i < n1; i++) {
					j = 0;
					while (j < n2) {
						if (values1[i] == values2[j]) {
							counter++;
						}
						j++;
					}
				}
				if (counter >= n1) {
					isTwice = true;
				}
			} else if (n1 < n2) {
				for (int i = 0; i < n1; i++) {
					j = 0;
					while (j < n2) {
						if (values1[i] == values2[j]) {
							counter++;
						}
						j++;
					}
				}
				if (counter >= n2) {
					isTwice = true;
				}
			}
		} else if (n1 == n2) {
			isTwice = true;
		}

		return isTwice;
	}
}
