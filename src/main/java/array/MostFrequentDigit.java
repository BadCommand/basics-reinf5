package array;

public class MostFrequentDigit {

	public static int getMostFrequentDigit(String digitString) {
		/*
		 * TODO Implement a method getMostFrequentDigit() that gets a string of digits
		 * (0 to 9) and then returns the most common digit. Example: 1 occurs most often
		 * in the string "13223111". If there are several digits with the same
		 * frequency, return the smallest of these digits. The string passed contains at
		 * least one digit, empty strings are not allowed.
		 */

		int mostCommon = 0;
		int counter;
		char checkStr;
		int tmp;
		char[] chars = digitString.toCharArray();
		int tmpOld = 0;
		int shortest = 1;
		int checkShortest = 0;

		// Nimm erste Stelle und schaue ob diese nochmals im Array vorkommt, wenn ja
		// zähle den counter hoch

		if (digitString != "") {
			for (int i = 0; i < chars.length; i++) {
				checkStr = chars[i];
				tmp = 0;
				counter = 0;

				while (counter < chars.length) {
					if (checkStr == chars[counter]) {
						tmp = tmp + 1;
					}
					checkShortest = Character.getNumericValue(checkStr);

					counter++;
				}
				
				if (checkShortest < shortest) {
					 shortest = checkShortest;
				}

				if (tmp > tmpOld) {
					mostCommon = Character.getNumericValue(checkStr);
					tmpOld = tmp;
				} 
				else if (tmp == tmpOld && chars.length <= 9)
					mostCommon = shortest;
			}
		}

		return mostCommon;
	}
}
