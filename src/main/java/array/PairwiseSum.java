package array;

public class PairwiseSum {

    public static int[] pairwiseSum(int[] values) {
        /* TODO Write a method pairwiseSum(), which takes 2 adjacent values from a given int array and
    	adds them up. The results are stored in a new int array and returned.
    	E.g.:

    	{1, 5, 10} => {6, 15}
    	{2,2} -> {4}
    	{3} -> {}
    	*/
    	if (values.length == 0)
    		return values;
    	
    	int tmp;
    	int[] newArray = new int[(values.length)-1];
    	
    	if (values.length > 1)
    	for(int i = 0; i < values.length-1; i++) {
    		tmp = values[i];
    		newArray[i] = tmp + values[i+1];
    	}
    	
    	
    	
        return newArray;
    }
}
