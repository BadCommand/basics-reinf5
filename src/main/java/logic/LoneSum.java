package logic;

public class LoneSum {

	public static int loneSum(int[] values) {
		/*
		 * TODO Given a Array of int values (all between 0 and 9), return their sum.
		 * However, ... iIf one of the values occurs more than once, it does not count
		 * towards the sum.
		 * 
		 * loneSum([1, 2, 3]) → 6 loneSum([3, 2, 3]) → 2 loneSum([3, 3, 3]) → 0
		 */

		int sum = 0;
		int n = values.length;

		if (n == 1) {
			sum = values[n - 1];
		} else if (n == 2) {
			if (values[n - 2] == values[n - 1]) {
				sum = 0;
			} else {
				sum = values[n - 2] + values[n - 1];
			}
		} else if (n == 3) {
			if (values[n - 3] == values[n - 2]) {
				if (values[n - 3] == values[n - 1]) {
					sum = 0;
				} else {
					sum = values[n - 1];
				}
			} else if (values[n - 3] == values[n - 1]) {
				sum = values[n - 2];
			} else {
				sum = values[n - 3] + values[n - 2] + values[n - 1];
			}
		} else if (n == 4) {
			if (values[n - 4] == values[n - 3]) {
				if (values[n - 4] == values[n - 2]) {
					if (values[n - 4] == values[n - 1]) {
						sum = 0;
					} else {
						sum = values[3];
					}
				} else if (values[n - 4] == values[n - 1]) {
					sum = values[n - 2];
				} else if (values[n - 2] == values[n - 1]) {
					sum = 0;
				} else {
					sum = values[n - 2] + values[n - 1];
				}
			} else if (values[n - 4] == values[n - 2]) {
				if (values[n - 4] == values[n - 1]) {
					sum = values[n - 3];
				} else {
					sum = values[n - 3] + values[n - 1];
				}
			} else if (values[n - 4] == values[n - 3]) {
				sum = values[n - 3] + values[n - 2];
			} else if (values[n - 3] == values[n - 2]) {
				if (values[n - 3] == values[n - 1]) {
					sum = values[n - 4];
				} else {
					sum = values[n - 4] + values[n - 1];
				}
			} else {
				sum = values[0] + values[1] + values[2] + values[3];
			}
		} else if (n == 5) {
			if (values[n - 5] == values[n - 4]) {
				if (values[n - 5] == values[n - 3]) {
					if (values[n - 5] == values[n - 2]) {
						if (values[n - 5] == values[n - 1]) {
							sum = 0;
						} else {
							sum = values[4];
						}
					} else if (values[n - 5] == values[n - 1]) {
						sum = values[3];
					} else {
						sum = values[3] + values[4];
					}
				}
			}
		}

		return sum;
	}

}
