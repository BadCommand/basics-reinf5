package logic;

public class ThreeDice {

	public static int getNbrOfCombinations(int sum) {

		/*
		 * TODO Implement a method that returns the number of possible combinations in
		 * which a red, a green and a blue die add up to a sum.
		 * 
		 * Example for sum = 17: The sum 17 is possible with {red = 5, green = 6, blue =
		 * 6} or {red = 6, green = 5, blue = 6} or {red = 6, green = 6, blue = 5}. So
		 * there are 3 possible combinations with a total of 17.
		 */

		//Source of main idea: https://slideplayer.com/slide/9046895/
		
		int numComb = 0;
		
		if (sum == 18 || sum == 3) {
			numComb = 1;	// 6 + 6 + 6 = 18; 1 + 1 + 1 = 3;
		}
		else if (sum == 4 || sum == 17) {
			numComb = 3;
		}
		else if (sum == 16 || sum == 5) {
			numComb = 6;
		}
		else if (sum == 15 || sum == 6) {
			numComb = 10; 
		}
		else if (sum == 14 || sum == 7) {
			numComb = 15;  
		}
		else if (sum == 13 || sum == 8) {
			numComb = 21;
		}
		else if (sum == 12 || sum == 9) {
			numComb = 25;
		}
		else if (sum == 11 || sum == 10) {
			numComb = 27;
		}

		return numComb;
	}
}