package logic;

public class LeapYear {

    public static boolean leapYear(int year) {
        /* TODO Implement a method that checks whether the year with the specified year number (for example, 2017)
    	is a leap year. The method returns 'true' if year is a leap year.
    	The rules for determining leap years are as follows:

    	A year is NOT a leap year if the year is NOT divisible by four.
    	A year is NOT a leap year if the year number is divisible by 100, unless the number is also divisible by 400.
    	In all other cases, the year shall be a leap year.
    	*/ 	
    	
    	boolean leapYear = ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0));

    	
    	
        return leapYear;
    }
}
