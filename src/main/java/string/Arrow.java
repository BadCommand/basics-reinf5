package string;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Arrow {

	/*
	int length = 5;		// length of arrow; pos. values = arrow from left to right / neg. values = from right to left
	boolean doubleEnded = true;		// number of arrowhead in characters
	boolean doubleLine = false;		// double or single arrow
	String arr = arrow(length, doubleEnded, doubleLine);
	 */

	public static String arrow(int length, boolean doubleEnded, boolean doubleLine) {
		// TODO Implement the method

		String taskSolved = "";
		int absLength = Math.abs(length);
		String[] arrow = new String[absLength];
		String arrowEnding[] = {"<", "<<", ">", ">>"};

		for (int i = 0; i < absLength; i++) {	

			if (doubleLine == true) {
				arrow[i] = "=";
			}
			else {
				arrow[i] = "-";
			}	
			
			taskSolved = taskSolved + arrow[i];
		}

		if (doubleEnded == true && length > 0) {
			taskSolved = String.format(taskSolved + "%s", arrowEnding[3]);
		}
		else if (doubleEnded == true && length < 0) {
			taskSolved = String.format("%s" + taskSolved, arrowEnding[1]);
		}
		else if (doubleEnded != true && length > 0) {
			taskSolved = String.format(taskSolved + "%s", arrowEnding[2]);
		}
		else if (doubleEnded != true && length < 0) {
			taskSolved = String.format("%s" + taskSolved, arrowEnding[0]);
		}
		else {
			taskSolved = "";
		}
		//System.out.println(taskSolved);

		return taskSolved;
	}
}
