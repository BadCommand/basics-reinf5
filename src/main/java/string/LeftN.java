package string;

import java.util.Arrays;

public class LeftN {

	public static String leftN(String str, int n) {
		// TODO Implement the method

		String rotStr = "";
		int strLength = str.length();
		char[] splStr = str.toCharArray();
		int Index;
		int counter = 0;

		if (n != 0 && n != strLength && n < strLength) {
			char[] takeIt = new char[n];

			for (int i = 0; i < n; i++) {
				takeIt[i] += splStr[i];
				Index = i;
				counter++;
			}

			char[] leftIt = new char[strLength - n];

			for (int j = 0; j < leftIt.length; j++) {

				leftIt[j] = splStr[j + counter];
			}

			String leftPart = new String(leftIt);
			String rightPart = new String(takeIt);

			rotStr = leftPart + rightPart;

		} else {
			rotStr = str;
		}

		return rotStr;
	}
}
