package string;

import java.util.stream.Collectors;

public class CountDogs {

	public static int countDogs(String input) {
		// TODO Implement the method

		String[] lookingFor = { "dag".toLowerCase(), "deg".toLowerCase(), "dig".toLowerCase(), "dug".toLowerCase(),
				"dog".toLowerCase() };

		int counter = 0;
		int index = 0;

		for (int i = 0; i < lookingFor.length; i++) {
			index = input.indexOf(lookingFor[i], index);

			while (index != -1) {

				if (index != -1) {
					counter++;
					index += lookingFor[i].length();
					break;
				}
			}
		}

		return counter;
	}

}
